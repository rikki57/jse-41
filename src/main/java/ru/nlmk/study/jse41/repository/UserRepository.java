package ru.nlmk.study.jse41.repository;

import java.sql.*;
import java.util.Optional;

public class UserRepository {
    private static final String URL = "jdbc:postgresql://localhost:5432/jse41";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "sa";

    public UserRepository() {

    }

    public Optional<User> getById(Long id){
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users where id = ?")) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(new User(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getString(3),resultSet.getInt(4)));
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public User createUser(User user){
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users (name, city, age) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, "Alexander");
            statement.setString(2, "Kazan");
            statement.setInt(3, 35);
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while (resultSet.next()){
                System.out.println(resultSet.getLong(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
