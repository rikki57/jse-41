package ru.nlmk.study.jse41.simple;

import java.sql.*;

public class MainTransact {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/jse41";
        String userName = "postgres";
        String password = "sa";
/*        try(Connection connection = DriverManager.getConnection(url, userName, password);
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users (name, city, age) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)){
            try {
                connection.setAutoCommit(false);
                statement.setString(1, "Alexander");
                statement.setString(2, "Kazan");
                statement.setInt(3, 35);
                statement.executeUpdate();
                statement.setString(1, "Igor");
                statement.setString(2, "Anapa");
                statement.setInt(3, 32);
                statement.executeUpdate();
                statement.setString(1, "Petr");
                statement.setString(2, "Vologda");
                statement.setInt(3, 19);
                statement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
            }
            connection.setAutoCommit(true);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }*/
        try(Connection connection = DriverManager.getConnection(url, userName, password);
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users (name, city, age) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)){
            try {
                connection.setAutoCommit(false);
                statement.setString(1, "Alexander");
                statement.setString(2, "Kazan");
                statement.setInt(3, 35);
                statement.addBatch();
                statement.setString(1, "Igor");
                statement.setString(2, "Anapa");
                statement.setInt(3, 32);
                statement.addBatch();
                statement.setString(1, "Petr");
                statement.setString(2, "Vologda");
                statement.setInt(3, 19);
                statement.addBatch();
                statement.executeBatch();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
            }
            connection.setAutoCommit(true);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
