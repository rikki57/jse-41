package ru.nlmk.study.jse41.simple;

import java.sql.*;

public class MainPrepared {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/jse41";
        String userName = "postgres";
        String password = "sa";
/*        try(Connection connection = DriverManager.getConnection(url, userName, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE name = ?")){
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.getString(2) + ", " + resultSet.getString(3));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }*/

        try(Connection connection = DriverManager.getConnection(url, userName, password);
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users (name, city, age) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, "Alexander");
            statement.setString(2, "Kazan");
            statement.setInt(3, 35);
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while (resultSet.next()){
                System.out.println(resultSet.getLong(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
