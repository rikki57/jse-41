package ru.nlmk.study.jse41.repository;

import java.sql.Connection;

public class ConnectionStore {
    private Connection connection;
    private boolean busy;

    public ConnectionStore() {
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }
}
